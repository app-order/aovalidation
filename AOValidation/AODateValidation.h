//
//  AODateValidation.h
//  AOValidation
//
//  Created by Anthony Miller on 8/4/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AODateValidation : NSObject

/**
 *  Validates that an `NSDate` is a date in the past.
 *
 *  @param date The date to validate
 *
 *  @return a boolean for the validation status of the date
 */
+ (BOOL)validatePastDate:(NSDate *)date;

/**
 *  Validates that an `NSDate` is a date in the future.
 *
 *  @param date The date to validate
 *
 *  @return a boolean for the validation status of the date
 */
+ (BOOL)validateFutureDate:(NSDate *)date;

@end
