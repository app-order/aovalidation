//
//  AODecimalNumberValidation.h
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AODecimalNumberValidation : NSObject

/**
 *  Validates that an `NSString` represents a number value that accepts a decimal number.
 *
 *  @param number The string to validate as a number
 *
 *  @return a boolean for the validation status of the string.
 */
+ (BOOL)validateDecimalNumber:(NSString *)decimalNumber;

@end
