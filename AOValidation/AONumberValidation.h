//
//  AONumberValidation.h
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AONumberValidation : NSObject

/**
 *  Validates that an `NSString` represents a non-decimal number value.
 *
 *  @param number The string to validate as a number
 *
 *  @return a boolean for the validation status of the string.
 */
+ (BOOL)validateNumber:(NSString *)number;

/**
 *  Validates that an `NSString` represents a non-decimal number value within a given range.
 *
 *  @param number The string to validate as a number
 *  @param max  The inclusive maximium for the number range
 *  @param min The inclusive minimum for the number range
 *
 *  @return a boolean for the validation status of the string.
 */
+ (BOOL)validateNumber:(NSString *)number inRangeWithMinimum:(NSNumber *)min maximum:(NSNumber *)max;

@end
