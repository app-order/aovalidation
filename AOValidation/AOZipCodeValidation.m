//
//  AOZipCodeValidation.m
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOZipCodeValidation.h"

#import "AOValidationHelper.h"

@implementation AOZipCodeValidation

+ (BOOL)validateZipCode:(NSString *)zipCode
{
  zipCode = [AOValidationHelper trimString:zipCode];
  
  NSMutableCharacterSet *zipCodeCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
  [zipCodeCharacterSet addCharactersInString:@"-"];
  
  long numberOfDashes = [[zipCode componentsSeparatedByString:@"-"] count]-1;
  
  BOOL isValidTenDigitZip = zipCode.length == 10 && [[zipCode substringWithRange:NSMakeRange(5,1)] isEqualToString: @"-"];
  
  if (zipCode.length == 5 ||
      zipCode.length == 9 ||
      isValidTenDigitZip) {
    return [AOValidationHelper string:zipCode validForCharacterSet:zipCodeCharacterSet] && numberOfDashes <= 1;
    
  }
  return NO;
}

@end
