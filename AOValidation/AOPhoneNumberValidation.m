//
//  AOPhoneNumberValidation.m
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOPhoneNumberValidation.h"

#import "AOValidationHelper.h"

static NSString * const AOPhoneNumberValidCharacters = @"1234567890";

@implementation AOPhoneNumberValidation

+ (BOOL)validatePhoneNumber:(NSString *)phoneNumber
{
  phoneNumber = [self formatPhoneNumber:phoneNumber];
  
  if (phoneNumber.length < 10 ||
      phoneNumber.length > 11  ||
      ![phoneNumber intValue] ||
      ![AOValidationHelper string:phoneNumber
             validForCharacterSet:[NSCharacterSet
                      characterSetWithCharactersInString:AOPhoneNumberValidCharacters]]) {
    return NO;
    
  }
  return YES;
}

+ (NSString *)formatPhoneNumber:(NSString *)phoneNumber
{
  phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
  phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
  phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
  phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
  phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
  phoneNumber = [AOValidationHelper trimString:phoneNumber];
  
  return phoneNumber;
}

@end
