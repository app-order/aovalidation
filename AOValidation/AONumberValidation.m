//
//  AONumberValidation.m
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AONumberValidation.h"

#import "AOValidationHelper.h"

@implementation AONumberValidation

+ (BOOL)validateNumber:(NSString *)number
{
  number = [AOValidationHelper trimString:number];
  
  return number.length >= 1 && [AOValidationHelper string:number
                       validForCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
}

+ (BOOL)validateNumber:(NSString *)number inRangeWithMinimum:(NSNumber *)min maximum:(NSNumber *)max
{
  if ([[self class] validateNumber:number]) {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *num = [formatter numberFromString:number];
    if (num >= min && num <= max) {
      return YES;
    }
    return NO;
  }
  return NO;
}

@end
