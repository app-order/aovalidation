//
//  AOContactValidation.h
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AOValidationHelper.h"

#import "AOEmailValidation.h"
#import "AOPhoneNumberValidation.h"
#import "AOZipCodeValidation.h"

@interface AOContactValidation : NSObject

@end
