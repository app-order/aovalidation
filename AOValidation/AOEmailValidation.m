//
//  AOEmailValidation.m
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOEmailValidation.h"

#import "AOValidationHelper.h"

@implementation AOEmailValidation

+ (BOOL)validateEmail:(NSString *)email
{
  NSString *emailRegex = @"[^@]+@[^.@]+(\\.[^.@]+)+";
  NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
  BOOL validEmail = [emailPredicate evaluateWithObject:[AOValidationHelper trimString:email]];
  return validEmail;
}

@end
