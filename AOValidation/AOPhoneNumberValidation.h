//
//  AOPhoneNumberValidation.h
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AOPhoneNumberValidation : NSObject

+ (BOOL)validatePhoneNumber:(NSString *)phoneNumber;

@end
