//
//  AOValidationHelper.h
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AOValidationHelper : NSObject

+ (BOOL)string:(NSString *)string validForCharacterSet:(NSCharacterSet *)validCharacters;

+ (NSString *)trimString:(NSString *)string;

@end
