//
//  AOValidationHelper.m
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOValidationHelper.h"

@implementation AOValidationHelper

+ (BOOL)string:(NSString *)string validForCharacterSet:(NSCharacterSet *)validCharacters
{
  NSCharacterSet *charactersInString = [NSCharacterSet characterSetWithCharactersInString:string];
  return [validCharacters isSupersetOfSet:charactersInString];
}

+ (NSString *)trimString:(NSString *)string
{
  NSCharacterSet *whiteSpaceSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
  return [string stringByTrimmingCharactersInSet:whiteSpaceSet];
}

@end
