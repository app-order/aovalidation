//
//  AODateValidation.m
//  AOValidation
//
//  Created by Anthony Miller on 8/4/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AODateValidation.h"

@implementation AODateValidation

+ (BOOL)validatePastDate:(NSDate *)date
{
  if ([date timeIntervalSinceNow] < 0.0) {
    return YES;
  }
  return NO;
}

+ (BOOL)validateFutureDate:(NSDate *)date
{
  if ([date timeIntervalSinceNow] >= 0.0) {
    return YES;
  }
  return NO;
}
@end
