//
//  AODecimalNumberValidation.m
//  AOValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AODecimalNumberValidation.h"

#import "AOValidationHelper.h"

@implementation AODecimalNumberValidation

+ (BOOL)validateDecimalNumber:(NSString *)decimalNumber
{
  decimalNumber = [AOValidationHelper trimString:decimalNumber];
  
  NSMutableCharacterSet *decimalNumberCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
  [decimalNumberCharacterSet addCharactersInString:@"."];
  
  long numberOfPeriods = [[decimalNumber componentsSeparatedByString:@"."] count]-1;
  if (numberOfPeriods <=1 ) {
    return decimalNumber.length >= 1 && [AOValidationHelper string:decimalNumber
                                validForCharacterSet:decimalNumberCharacterSet];
  }
  return NO;
}

@end
