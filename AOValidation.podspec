Pod::Spec.new do |s|
  s.platform     = :ios
  s.ios.deployment_target = '7.0'
  s.name         = "AOValidation"
  s.version      = "0.3.2"
  s.summary      = "Methods for validation of NSStrings for numbers, emails, phone numbers, zip codes, etc. ."
  s.homepage     = "http://redmine.app-order.com"
  s.license      = { :type => "Closed Source", :file => "LICENSE.txt" }
  s.author       = { "Anthony Miller" => "anthony@app-order.com" }
  s.source   	 = { :git => "https://bitbucket.org/app-order/aovalidation.git",
                     :tag => "#{s.version}"}
  s.framework = "UIKit"
  s.requires_arc = true

  s.source_files = "AOValidation/*.{h,m}"
end