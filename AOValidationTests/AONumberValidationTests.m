//
//  AONumberValidationTests.m
//  AONumberValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

// Test Class
#import "AONumberValidation.h"

// Collaborators

// Test Support
#import <XCTest/XCTest.h>

#define EXP_SHORTHAND YES
#import <Expecta/Expecta.h>

#import <OCMock/OCMock.h>

@interface AONumberValidationTests : XCTestCase
@end

@implementation AONumberValidationTests
{
  
}

#pragma mark - Test Lifecycle

- (void)setUp
{
  [super setUp];
}

#pragma mark - Vaidate Number Tests

- (void)test___validateNumber___givenValidNumber_returns_YES
{
  // given
  NSString *testString = @"12345";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateNumber___givenValidNumber_WithDecimal_returns_NO
{
  // given
  NSString *testString = @"12.345";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateNumber___givenEmptyString_returns_NO
{
  // given
  NSString *testString = @"";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateNumber___givenStringWithLetters_returns_NO
{
  // given
  NSString *testString = @"12A345";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateNumber___givenStringWithSymbol_returns_NO
{
  // given
  NSString *testString = @"12^345";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateNumber___givenStringWithWhiteSpace_returns_YES
{
  // given
  NSString *testString = @"  \n 12345  \n ";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

#pragma mark - Validate Number In Range - Tests

- (void)test___validateNumber_inRange_givenNumberWithinRange_returns_YES
{
  // given
  NSString *testString = @"7";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString inRangeWithMinimum:@1 maximum:@10];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateNumber_inRange_givenNumberNotWithinRange_returns_NO
{
  // given
  NSString *testString = @"15";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString inRangeWithMinimum:@1 maximum:@10];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateNumber_inRange_givenNotANumber_returns_NO
{
  // given
  NSString *testString = @"A";
  
  // when
  BOOL result = [AONumberValidation validateNumber:testString inRangeWithMinimum:@1 maximum:@10];
  
  // then
  expect(result).to.equal(NO);
}


@end