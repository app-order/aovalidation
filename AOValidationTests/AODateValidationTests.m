//
//  AODateValidationTests.m
//  AOValidation
//
//  Created by Anthony Miller on 8/4/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

// Test Class
#import "AODateValidation.h"

// Collaborators

// Test Support
#import <XCTest/XCTest.h>

#define EXP_SHORTHAND YES
#import <Expecta/Expecta.h>

//#import <OCMock/OCMock.h>

@interface AODateValidationTests : XCTestCase
@end

@implementation AODateValidationTests
{
  
}

#pragma mark - Test Lifecycle

- (void)setUp
{
  [super setUp];
}

#pragma mark - Date Validation - Tests

- (void)test___validatePastDate___givenPastDate_returnsYES
{
  // given
  NSDate *date = [NSDate distantPast];
  
  // then
  expect([AODateValidation validatePastDate:date]).to.equal(YES);
}

- (void)test___validatePastDate___givenFutureDate_returnsNO
{
  // given
  NSDate *date = [NSDate distantFuture];
  
  // then
  expect([AODateValidation validatePastDate:date]).to.equal(NO);
}

- (void)test___validateFutureDate___givenFutureDate_returnsYES
{
  // given
  NSDate *date = [NSDate distantFuture];
  
  // then
  expect([AODateValidation validateFutureDate:date]).to.equal(YES);
}

- (void)test___validateFutureDate___givenPastDate_returnsNO
{
  // given
  NSDate *date = [NSDate distantPast];
  
  // then
  expect([AODateValidation validateFutureDate:date]).to.equal(NO);
}
@end