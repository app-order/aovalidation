//
//  AOZipCodeValidationTests.m
//  AOZipCodeValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

// Test Class
#import "AOZipCodeValidation.h"

// Collaborators

// Test Support
#import <XCTest/XCTest.h>

#define EXP_SHORTHAND YES
#import <Expecta/Expecta.h>

#import <OCMock/OCMock.h>

@interface AOZipCodeValidationTests : XCTestCase
@end

@implementation AOZipCodeValidationTests
{

}

#pragma mark - Test Lifecycle

- (void)setUp
{
  [super setUp];
}

#pragma mark - Validate Zip Code Tests

- (void)test___validateZipCode___givenValidFiveDigitZip_returns_YES
{
  // given
  NSString *testString = @"12345";
  
  // when
  BOOL result = [AOZipCodeValidation validateZipCode:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateZipCode___givenValidNineDigitZip_returns_YES
{
  // given
  NSString *testString = @"12345-6789";
  
  // when
  BOOL result = [AOZipCodeValidation validateZipCode:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateZipCode___givenNineDigitZipNoDash_returns_YES
{
  // given
  NSString *testString = @"123456789";
  
  // when
  BOOL result = [AOZipCodeValidation validateZipCode:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateZipCode___givenTooManyDigits_returns_NO
{
  // given
  NSString *testString = @"1234567890";
  
  // when
  BOOL result = [AOZipCodeValidation validateZipCode:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateZipCode___givenNotEnoughDigits_returns_NO
{
  // given
  NSString *testString = @"1234";
  
  // when
  BOOL result = [AOZipCodeValidation validateZipCode:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateZipCode___givenStringBetweenFiveAndNineDigits_returns_NO
{
  // given
  NSString *testString = @"12345-67";
  
  // when
  BOOL result = [AOZipCodeValidation validateZipCode:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateZipCode___givenTwoDashes_returns_NO
{
  // given
  NSString *testString = @"12345-67-9";
  
  // when
  BOOL result = [AOZipCodeValidation validateZipCode:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateZipCode___givenEmptyString_returns_NO
{
  // given
  NSString *testString = @"";
  
  // when
  BOOL result = [AOZipCodeValidation validateZipCode:testString];
  
  // then
  expect(result).to.equal(NO);
}

@end