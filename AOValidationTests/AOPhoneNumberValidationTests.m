//
//  AOPhoneNumberValidationTests.m
//  AOPhoneNumberValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

// Test Class
#import "AOPhoneNumberValidation.h"

// Collaborators

// Test Support
#import <XCTest/XCTest.h>

#define EXP_SHORTHAND YES
#import <Expecta/Expecta.h>

#import <OCMock/OCMock.h>

@interface AOPhoneNumberValidationTests : XCTestCase
@end

@implementation AOPhoneNumberValidationTests
{
  
}

#pragma mark - Test Lifecycle

- (void)setUp
{
  [super setUp];
}

#pragma mark - Validate Phone Number Tests

- (void)test___validatePhoneNumber___givenValidPhoneNumberWithTenDigits_returns_YES
{
  // given
  NSString *testString = @"1234567890";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validatePhoneNumber___givenValidPhoneNumberWithElevenDigits_returns_YES
{
  // given
  NSString *testString = @"12345678901";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validatePhoneNumber___givenValidPhoneNumber_withDashes_returns_YES
{
  // given
  NSString *testString = @"123-456-7890";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validatePhoneNumber___givenValidPhoneNumber_withPlusSign_returns_YES
{
  // given
  NSString *testString = @"+11234567890";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validatePhoneNumber___givenValidPhoneNumber_withParentheses_returns_YES
{
  // given
  NSString *testString = @"(123) 4567890";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validatePhoneNumber___givenValidPhoneNumber_withParentheses_andDashes_returns_YES
{
  // given
  NSString *testString = @"(123) 456-7890";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validatePhoneNumber___givenValidPhoneNumber_withWhiteSpace_returns_YES
{
  // given
  NSString *testString = @"  \n 1234567890  \n ";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validatePhoneNumber___givenValidPhoneNumber_withSpaces_returns_YES
{
  // given
  NSString *testString = @"123 456 7890";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validatePhoneNumber___givenInvalidPhoneNumber_withLetters_returns_NO
{
  // given
  NSString *testString = @"123A456a78";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validatePhoneNumber___givenInvalidPhoneNumber_withSymbol_returns_NO
{
  // given
  NSString *testString = @"123$45678";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validatePhoneNumber___givenInvalidPhoneNumber_tooShort_returns_NO
{
  // given
  NSString *testString = @"123456789";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validatePhoneNumber___givenInvalidPhoneNumber_tooLong_returns_NO
{
  // given
  NSString *testString = @"123456789012";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validatePhoneNumber___givenInvalidPhoneNumber_emptyString_returns_NO
{
  // given
  NSString *testString = @"";
  
  // when
  BOOL result = [AOPhoneNumberValidation validatePhoneNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

@end