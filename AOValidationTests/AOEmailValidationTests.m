//
//  AOEmailValidationTests.m
//  AOEmailValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

// Test Class
#import "AOEmailValidation.h"

// Collaborators

// Test Support
#import <XCTest/XCTest.h>

#define EXP_SHORTHAND YES
#import <Expecta/Expecta.h>

#import <OCMock/OCMock.h>

@interface AOEmailValidationTests : XCTestCase
@end

@implementation AOEmailValidationTests
{
  
}

#pragma mark - Test Lifecycle

- (void)setUp
{
  [super setUp];
}

#pragma mark - Email Validation - Tests

- (void)test___validateEmail___givenValidEmail_returns_YES
{
  // given
  NSString *testString = @"test@test.com";
  
  // when
  BOOL result = [AOEmailValidation validateEmail:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateEmail___givenEmptyString_returns_NO
{
  // given
  NSString *testString = @"";
  
  // when
  BOOL result = [AOEmailValidation validateEmail:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateEmail___givenStringWithNoPrefix_returns_NO
{
  // given
  NSString *testString = @"test.com";
  
  // when
  BOOL result = [AOEmailValidation validateEmail:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateEmail___givenStringWithNoDomain_returns_NO
{
  // given
  NSString *testString = @"test@.com";
  
  // when
  BOOL result = [AOEmailValidation validateEmail:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateEmail___givenStringWithNoSuffix_returns_NO
{
  // given
  NSString *testString = @"test@test";
  
  // when
  BOOL result = [AOEmailValidation validateEmail:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateEmail___givenValidEmail_withWhiteSpace_returns_YES
{
  // given
  NSString *testString = @"  \n test@test.com   \n  ";
  
  // when
  BOOL result = [AOEmailValidation validateEmail:testString];
  
  // then
  expect(result).to.equal(YES);
}

@end