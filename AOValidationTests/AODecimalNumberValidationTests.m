//
//  AODecimalNumberValidationTests.m
//  AODecimalNumberValidation
//
//  Created by Anthony Miller on 6/26/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

// Test Class
#import "AODecimalNumberValidation.h"

// Collaborators

// Test Support
#import <XCTest/XCTest.h>

#define EXP_SHORTHAND YES
#import <Expecta/Expecta.h>

#import <OCMock/OCMock.h>

@interface AODecimalNumberValidationTests : XCTestCase
@end

@implementation AODecimalNumberValidationTests
{

}

#pragma mark - Test Lifecycle

- (void)setUp
{
  [super setUp];
}

#pragma mark - Validate Decimal Number Tests

- (void)test___validateDecimalNumber___givenValidDecimalNumber_returns_YES
{
  // given
  NSString *testString = @"12.34567890";
  
  // when
  BOOL result = [AODecimalNumberValidation validateDecimalNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateDecimalNumber___givenValidDecimalNumberWithWhiteSpace_returns_YES
{
  // given
  NSString *testString = @" \n12.34567890  \n";
  
  // when
  BOOL result = [AODecimalNumberValidation validateDecimalNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateDecimalNumber___givenValidNumberWithNoDecimal_returns_YES
{
  // given
  NSString *testString = @"1234567890";
  
  // when
  BOOL result = [AODecimalNumberValidation validateDecimalNumber:testString];
  
  // then
  expect(result).to.equal(YES);
}

- (void)test___validateDecimalNumber___givenNumberWithTwoDecimal_returns_NO
{
  // given
  NSString *testString = @"12.34567.890";
  
  // when
  BOOL result = [AODecimalNumberValidation validateDecimalNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateDecimalNumber___givenStringWithSymbol_returns_NO
{
  // given
  NSString *testString = @"1234567%890";
  
  // when
  BOOL result = [AODecimalNumberValidation validateDecimalNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

- (void)test___validateDecimalNumber___givenEmptyString_returns_NO
{
  // given
  NSString *testString = @"";
  
  // when
  BOOL result = [AODecimalNumberValidation validateDecimalNumber:testString];
  
  // then
  expect(result).to.equal(NO);
}

@end